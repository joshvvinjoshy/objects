function mapo(obj, cb){
    for(const k in obj){
        obj[k]=cb(obj[k]);
    }
    return obj;
}
module.exports = mapo;