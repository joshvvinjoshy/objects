function defaults(obj, defobj){
    for(const k in defobj){
        if(obj[k] == undefined && defobj[k] != undefined){
            obj[k]=defobj[k];
            
        }
    }
    return obj;
}
module.exports = defaults